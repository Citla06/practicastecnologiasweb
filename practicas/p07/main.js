function validarNombre(control)
        {
            if( control.value == '' ){
                alert("Debe ingresar un nombre");
            }
            else{
                if(control.value.length > 100){
                    alert ("El nombre es demasiado largo");
                }
            }
        }

function validarModelo(control)
{
    if( control.value == '' ){
        alert("Debe ingresar el modelo");
    }
    else{
        if(control.value.length > 25){
            alert("El modelo es demasiado largo");
        }
    }   
}

function validarPrecio(control){
    if( control.value == '' ){
        alert("Debe ingresar un precio");
    }
    else{
        num = parseFloat(control.value);
        if( num < 99.99){
            alert("Precio invalido, debe ser mayor a 99.99");
        }
    }
}

function validarDetalles(control){
    if(control.value.length > 250){
        alert("Solo se permiten 250 caracteres");
    }
}

function validarUnidades(control){
    if( control.value == '' ){
        alert("Debe ingresar las unidades");
    }
    else{
        num = parseInt(control.value);
        if( num < 0){
            alert("Unidades no validas, deben ser mayor a 0");
        }
    }
}

function validarImagen(control)
{
    if(control.value == ""){
        control.value = "img/img.png";
    }
}

function validarMarca(control)
{
    if(control.value == ""){
        alert("Se debe seleccionar una opción");
    }
}

function show() {
    // se obtiene el id de la fila donde está el botón presinado
    var rowId = event.target.parentNode.parentNode.id;

    // se obtienen los datos de la fila en forma de arreglo
    var data = document.getElementById(rowId).querySelectorAll(".row-data");
    /**
    querySelectorAll() devuelve una lista de elementos (NodeList) que 
    coinciden con el grupo de selectores CSS indicados.
    (ver: https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Selectors)

    En este caso se obtienen todos los datos de la fila con el id encontrado
    y que pertenecen a la clase "row-data".
    */
    var id = data[0].innerHTML;
    var nombre = data[1].innerHTML;
    var marca = data[2].innerHTML;
    var modelo = data[3].innerHTML;
    var precio = data[4].innerHTML;
    var unidades = data[5].innerHTML;
    var detalles = data[6].innerHTML;
    alert("Name: " + nombre + "\n\nimagen:" + imagen);
    var imagen = data[7].getAttribute("src");

    alert("Name: " + nombre + "\n\nimagen:" + imagen);

    send2form(id,nombre,marca, modelo, precio, unidades, detalles, imagen);
}

function send2form(id, nombre, marca, modelo, precio, unidades, detalles, imagen) {
    var form = document.createElement("form");

    var idIn = document.createElement("input");
    idIn.type = 'text';
    idIn.name = 'id';
    idIn.value = id;
    form.appendChild(idIn)

    var nombreIn = document.createElement("input");
    nombreIn.type = 'text';
    nombreIn.name = 'nombre';
    nombreIn.value = nombre;
    form.appendChild(nombreIn);

    var marcaIn = document.createElement("input");
    marcaIn.type = 'text';
    marcaIn.name = 'marca';
    marcaIn.value = marca;
    form.appendChild(marcaIn);

    var modeloIn = document.createElement("input");
    modeloIn.type = 'text';
    modeloIn.name = 'modelo';
    modeloIn.value = modelo;
    form.appendChild(modeloIn);

    var precioIn = document.createElement("input");
    precioIn.type = 'text';
    precioIn.name = 'precio';
    precioIn.value = precio;
    form.appendChild(precioIn);

    var unidadesIn = document.createElement("input");
    unidadesIn.type = 'text';
    unidadesIn.name = 'unidades';
    unidadesIn.value = unidades;
    form.appendChild(unidadesIn);

    var detallesIn = document.createElement("input");
    detallesIn.type = 'text';
    detallesIn.name = 'detalles';
    detallesIn.value = detalles;
    form.appendChild(detallesIn);

    var imagenIn = document.createElement("input");
    imagenIn.type = 'text';
    imagenIn.name = 'imagen';
    imagenIn.value = imagen;
    form.appendChild(imagenIn);

    console.log(form);

    form.method = 'POST';
    form.action = './formulario_producto_v2.php';  

    document.body.appendChild(form);
    form.submit();
}