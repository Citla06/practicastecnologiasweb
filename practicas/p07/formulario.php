<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>Practica 7 - Formulario</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"/>
	</head>
	<body>
    <body>
    <h1>Registro de Productos </h1>

    <p>Ingresa las caractericas de tu producto</p>

    <form id="formulario"  method="post">

        <fieldset>
            <ul>
            <li><label for="form-nombre">Nombre:</label> <input type="text" name="nombre_producto" id="form-nombre" onBlur="validarNombre(this)" require></li>
            <li><label for="form-marca">Marca:</label> 
            <input list="Marca" name = "form-marca" onBlur="validarMarca(this)">
            <datalist id="Marca">
                <option value="Amarillo"> Amarillo
                <option value="Verde"> Verde
                <option value="Rojo"> Rojo
                <option value="Morado"> Morado
                <option value="Azul"> Azul
                <option value="Gris"> Gris
            </datalist> </li>
            <li><label for="form-modelo">Modelo:</label> <input type="text" name="modelo_producto" id="form-modelo" onBlur="validarModelo(this)" require></li>
            <li><label for="form-precio">Precio:</label> <input type="number" name="precio_producto" id="form-precio" onBlur="validarPrecio(this)" require></li>
            <li><label for="form-detalles">Detalles del producto:</label><br><textarea name="detalles_producto" rows="5" cols="60" id="form-detalles" placeholder="Caracteristicas" onBlur="validarDetalles(this)"></textarea></li>
            <li><label for="form-unidades">Unidades:</label> <input type="number" name="unidades_producto" id="form-unidades" onBlur="validarUnidades(this)" require></li>
            <li><label for="form-precio">Imagen:</label> <input type="text" name="imagen" id="form-imagen" onBlur="validarImagen(this)"></li>
            </ul>
        </fieldset>

        <p>
            <input type="submit" value="Enviar">
        </p>
    </form>
    <script src = "./main.js">  </script>
	</body>
</html>
