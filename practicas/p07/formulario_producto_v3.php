<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>Practica 7 - Formulario</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"/>
	</head>
	<body>
    <body>
    <h1>Actualizar </h1>

    <form id="formulario" action="./actualizar.php" method="POST">

        <fieldset>
            <ul>
            <li><label for="form-id">ID:</label> <input type="text" name="id_producto" id="form-id" value="<?= !empty($_POST['id'])?$_POST['id']:$_GET['id'] ?>" readonly></li>
            <li><label for="form-nombre">Nombre:</label> <input type="text" name="nombre_producto" id="form-nombre" onBlur="validarNombre(this)" value="<?= !empty($_POST['nombre'])?$_POST['nombre']:$_GET['nombre'] ?>" required></li>
            <li><label for="form-marca">Marca:</label> 
            <input list="Marca" name="form-marca" onBlur="validarMarca(this)" value="<?= !empty($_POST['marca'])?$_POST['marca']:$_GET['marca'] ?>">
            <datalist id="Marca">
                <option value="Sony"> Sony </option>
                <option value="Amazon"> Amazon </option>
                <option value="Logitech"> Logitech </option>
                <option value="Vagalbox"> Vagalbox  </option>
                <option value="Razen"> Razen </option>
            </datalist> </li>
            <li><label for="form-modelo">Modelo:</label> <input type="text" name="modelo_producto" id="form-modelo" onBlur="validarModelo(this)" value="<?= !empty($_POST['modelo'])?$_POST['modelo']:$_GET['modelo'] ?>" required></li>
            <li><label for="form-precio">Precio:</label> <input type="number" name="precio_producto" id="form-precio" onBlur="validarPrecio(this)" value="<?= !empty($_POST['precio'])?$_POST['precio']:$_GET['precio'] ?>" required></li>
            <li><label for="form-detalles">Detalles del producto:</label><br><input Type="text" size="50" name="detalles_producto" rows="5" cols="60" id="form-detalles" placeholder="Caracteristicas" onBlur="validarDetalles(this)" value="<?= !empty($_POST['detalles'])?$_POST['detalles']:$_GET['detalles'] ?>"/> </li>
            <li><label for="form-unidades">Unidades:</label> <input type="number" name="unidades_producto" id="form-unidades" onBlur="validarUnidades(this)" value="<?= !empty($_POST['unidades'])?$_POST['unidades']:$_GET['unidades'] ?>" require></li>
            <li><label for="form-precio">Imagen:</label> <input type="text" name="imagen" id="form-imagen" onBlur="validarImagen(this)" value="<?= !empty($_POST['imagen'])?$_POST['imagen']:$_GET['imagen'] ?>"></li>
            </ul>
        </fieldset>

        <p>
            <input type="submit" value="Actualizar" >
        </p>

    </form>
    <script src = "./main.js">  </script>
	</body>
</html>
