<?php
    include_once __DIR__.'/database.php';

    $id = $_POST['id'];
    
    $query = "SELECT * FROM productos WHERE id = {$id}";
    $result = $conexion->query($query);
    if(!$result){
        die('Error de query');
    }

    $json = array();
    while($row = $result->fetch_array()) {
        $json[] = array(
            'id' => $row['id'],
            'nombre' => $row['nombre'],
            'precio' => $row['precio'],
            'unidades' => $row['unidades'],
            'modelo' => $row['modelo'],
            'marca' => $row['marca'],
            'detalles' => $row['detalles'],
            'imagen' => $row['imagen']
        );
    }

    $jsonstring = json_encode($json[0]);
    echo $jsonstring;
?>