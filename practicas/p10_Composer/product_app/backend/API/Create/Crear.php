<?php 
    namespace API\Create;
    
    use  API\Database;
    require_once __DIR__.'/../database.php';

    class Crear extends Database{

        public function add($post){
            $id = $post['id'];
            $nombre = $post['name'];
            $marca  = $post['marca'];
            $modelo = $post['modelo'];
            $precio = $post['precio'];
            $detalles = $post['detalles'];
            $unidades = $post['unidades'];
            $imagen   = $post['imagen'];
            
            $this->response = array(
                'status'  => 'error',
                'message' => 'Ya existe un producto con ese nombre'
            );

            //SE ASUME QUE LOS DATOS YA FUERON VALIDADOS ANTES DE ENVIARSE
            $sql = "SELECT * FROM productos WHERE nombre = '{$nombre}' AND eliminado = 0";
            $result = $this->conexion->query($sql);
            
            if ($result->num_rows == 0) {
                $this->conexion->set_charset("utf8");
                $sql = "INSERT INTO productos VALUES (null, '{$nombre}', '{$marca}','{$modelo}','{$precio}','{$detalles}','{$unidades}','{$imagen}', 0)";
                if($this->conexion->query($sql)){
                    $this->response['status'] =  "success";
                    $this->response['message'] =  "Producto agregado";
                } else {
                    $this->response['message'] = "ERROR: No se ejecuto $sql. " . mysqli_error($this->conexion);
                }
            
            // Cierra la conexion
            $this->conexion->close();
            }
        }
    }
?>