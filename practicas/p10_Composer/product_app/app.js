// VALIDACIONES
function validarNombre(control)
{
    if( control.value == '' ){
        let template_bar = '';
        template_bar += `
                    <li style="list-style: none;">Campo vacío. Debe ingresar un nombre de producto</li>
                `;
        // SE HACE VISIBLE LA BARRA DE ESTADO
        document.getElementById("product-result").className = "card my-4 d-block";
        // SE INSERTA LA PLANTILLA PARA LA BARRA DE ESTADO
        document.getElementById("container").innerHTML = template_bar; 
    }
    else if(control.value.length > 100){
            let template_bar = '';
            template_bar += `
                        <li style="list-style: none;">El nombre del producto es demasiado largo</li>
                    `;
            // SE HACE VISIBLE LA BARRA DE ESTADO
            document.getElementById("product-result").className = "card my-4 d-block";
            // SE INSERTA LA PLANTILLA PARA LA BARRA DE ESTADO
            document.getElementById("container").innerHTML = template_bar; 
    }
    else{
        if(control.value.length < 100 && control.value != '' ){
            let template_bar = '';
            template_bar += `
                        <li style="list-style: none;"> Nombre correcto </li>
                    `;
            // SE HACE VISIBLE LA BARRA DE ESTADO
            document.getElementById("product-result").className = "card my-4 d-block";
            // SE INSERTA LA PLANTILLA PARA LA BARRA DE ESTADO
            document.getElementById("container").innerHTML = template_bar; 
        }
    }
}

function validarModelo(control)
{
    if( control.value == '' ){
        let template_bar = '';
        template_bar += `
                    <li style="list-style: none;">Campo vacío. Debe ingresar el modelo</li>
                `;
        // SE HACE VISIBLE LA BARRA DE ESTADO
        document.getElementById("product-result").className = "card my-4 d-block";
        // SE INSERTA LA PLANTILLA PARA LA BARRA DE ESTADO
        document.getElementById("container").innerHTML = template_bar; 
    }
    else{
        if(control.value.length > 25){
            let template_bar = '';
        template_bar += `
                    <li style="list-style: none;">El modelo es demasiado largo.</li>
                `;
        // SE HACE VISIBLE LA BARRA DE ESTADO
        document.getElementById("product-result").className = "card my-4 d-block";
        // SE INSERTA LA PLANTILLA PARA LA BARRA DE ESTADO
        document.getElementById("container").innerHTML = template_bar; 
        }   
    }
}

function validarPrecio(control){
    if( control.value == '' ){
        let template_bar = '';
        template_bar += `
                    <li style="list-style: none;">Campo vacío. Debe ingresar el precio del producto.</li>
                `;
        // SE HACE VISIBLE LA BARRA DE ESTADO
        document.getElementById("product-result").className = "card my-4 d-block";
        // SE INSERTA LA PLANTILLA PARA LA BARRA DE ESTADO
        document.getElementById("container").innerHTML = template_bar; 
        }
    else{
        num = parseFloat(control.value);
        if( num < 99.99){
            let template_bar = '';
            template_bar += `
                        <li style="list-style: none;">Precio invalido. Debe ser mayor a $99.99</li>
                    `;
            // SE HACE VISIBLE LA BARRA DE ESTADO
            document.getElementById("product-result").className = "card my-4 d-block";
            // SE INSERTA LA PLANTILLA PARA LA BARRA DE ESTADO
            document.getElementById("container").innerHTML = template_bar; 
            }
    }
}

function validarDetalles(control){
    if(control.value.length > 250){
        let template_bar = '';
        template_bar += `
                    <li style="list-style: none;">Solo se permiten 250 caracteres</li>
                `;
        // SE HACE VISIBLE LA BARRA DE ESTADO
        document.getElementById("product-result").className = "card my-4 d-block";
        // SE INSERTA LA PLANTILLA PARA LA BARRA DE ESTADO
        document.getElementById("container").innerHTML = template_bar; 
    }
}

function validarUnidades(control){
    if( control.value == '' ){
            let template_bar = '';
            template_bar += `
                        <li style="list-style: none;">Campo vacío. Debe ingresar unidades del producto.</li>
                    `;
            // SE HACE VISIBLE LA BARRA DE ESTADO
            document.getElementById("product-result").className = "card my-4 d-block";
            // SE INSERTA LA PLANTILLA PARA LA BARRA DE ESTADO
            document.getElementById("container").innerHTML = template_bar; 
    }
    else{
        num = parseInt(control.value);
        if( num <= 0){
            let template_bar = '';
                template_bar += `
                            <li style="list-style: none;">Unidades no validas, deben ser mayor a 0.</li>
                        `;
                // SE HACE VISIBLE LA BARRA DE ESTADO
                document.getElementById("product-result").className = "card my-4 d-block";
                // SE INSERTA LA PLANTILLA PARA LA BARRA DE ESTADO
                document.getElementById("container").innerHTML = template_bar; 
        }
    }
}

function validarImagen(control)
{
    if(control.value == ""){
    control.value = "img/img.png";
    }
}

function validarMarca(control)
{
    if(control.value == ""){
        let template_bar = '';
            template_bar += `
                        <li style="list-style: none;">Campo vacío. Debe seleccionar o ingresar una opción.</li>
                    `;
            // SE HACE VISIBLE LA BARRA DE ESTADO
            document.getElementById("product-result").className = "card my-4 d-block";
            // SE INSERTA LA PLANTILLA PARA LA BARRA DE ESTADO
            document.getElementById("container").innerHTML = template_bar; 
    }
}

function validarCampos(control)
{
    if(control.value == ""){
        let template_bar = '';
            template_bar += `
                        <li style="list-style: none;">Campos vacíos. Debe llenar los campos requeridos.</li>
                    `;
            // SE HACE VISIBLE LA BARRA DE ESTADO
            document.getElementById("product-result").className = "card my-4 d-block";
            // SE INSERTA LA PLANTILLA PARA LA BARRA DE ESTADO
            document.getElementById("container").innerHTML = template_bar; 
    }
    else{
        let template_bar = '';
            template_bar += `
                        <li style="list-style: none;"> Formulario Correcto .</li>
                    `;
            // SE HACE VISIBLE LA BARRA DE ESTADO
            document.getElementById("product-result").className = "card my-4 d-block";
            // SE INSERTA LA PLANTILLA PARA LA BARRA DE ESTADO
            document.getElementById("container").innerHTML = template_bar; 
    }
}

$(document).ready(function(){
        let edit = false;
        $('#product-result').hide();//Oculta la barra de estado con jquery
        console.log('jQuery is working');
        //LISTAR LOS PRODUCTOS
        listaProducto();
        $('#search').keyup(function(e) {
            if($('#search').val()){
                let search = $('#search').val();
                $.ajax({
                    url:'./backend/product-search.php',
                    type:'GET',
                    data: { search },
                    success: function(response){
                        let productos = JSON.parse(response);
                        let template = '';
                        let template_bar = '';
                        productos.forEach(producto => {
                            let descripcion = '';
                            descripcion += '<li>precio: '+producto.precio+'</li>';
                            descripcion += '<li>unidades: '+producto.unidades+'</li>';
                            descripcion += '<li>modelo: '+producto.modelo+'</li>';
                            descripcion += '<li>marca: '+producto.marca+'</li>';
                            descripcion += '<li>detalles: '+producto.detalles+'</li>';
                        
                            template += `
                                <tr productId="${producto.id}">
                                    <td>${producto.id}</td>
                                    <td>${producto.nombre}</td>
                                    <td><ul>${descripcion}</ul></td>
                                    <td>
                                        <button class="product-delete btn btn-danger" onclick="eliminarProducto()">
                                            Eliminar
                                        </button>
                                    </td>
                                </tr>
                            `;

                            template_bar += `
                                <li>${producto.nombre}</il>
                            `;
                        });
                        // SE HACE VISIBLE LA BARRA DE ESTADO
                        document.getElementById("product-result").className = "card my-4 d-block";
                        // SE INSERTA LA PLANTILLA PARA LA BARRA DE ESTADO
                        document.getElementById("container").innerHTML = template_bar;  
                        // SE INSERTA LA PLANTILLA EN EL ELEMENTO CON ID "productos"
                        document.getElementById("products").innerHTML = template;
                    }
            });
            }
        })

        // AGREGAR DATOS
        $('#product-form').submit(function(e){
            //console.log('Enviando');
            const postData = {
                name: $('#form-nombre').val(), //# elemento de html por su id
                id: $('#form-id').val(),
                marca: $('#form-marca').val(),
                modelo: $('#form-modelo').val(),
                precio: $('#form-precio').val(),
                detalles: $('#form-detalles').val(),
                unidades: $('#form-unidades').val(),
                imagen: $('#form-imagen').val(),
            };
            

            let url = edit == false ? './backend/product-add.php' : './backend/product-edit.php';
            //console.log(url);
            
            $.post(url, postData, function(response) {
                //console.log('Esta es la respuesta de editar....');
            //console.log(response);
            let respuesta = JSON.parse(response); // Se recibe como string y se vuelve un objeto
            // SE CREA UNA PLANTILLA PARA CREAR INFORMACIÓN DE LA BARRA DE ESTADO
            let template_bar = '';
            template_bar += `
                        <li style="list-style: none;">status: ${respuesta.status}</li>
                        <li style="list-style: none;">message: ${respuesta.message}</li>
                    `;
            
            // SE HACE VISIBLE LA BARRA DE ESTADO con jquery
            $('#product-result').show();
            // SE INSERTA LA PLANTILLA PARA LA BARRA DE ESTADO con jquery
            $("#container").html(template_bar);   
            listaProducto();
            //$('#product-form').trigger('reset');
            });
            e.preventDefault();
        });


        //LISTAR LOS PRODUCTOS
        function listaProducto(){
            $.ajax({
                url: './backend/product-list.php',
                type: 'GET',
                success: function(response) {
                    console.log(response);
                    const productos = JSON.parse(response);
                    console.log(productos);
                    //console.log(response);
                    let template = '';
                    productos.forEach(producto => {
                            let descripcion = '';
                            descripcion += '<li>precio: '+producto.precio+'</li>';
                            descripcion += '<li>unidades: '+producto.unidades+'</li>';
                            descripcion += '<li>modelo: '+producto.modelo+'</li>';
                            descripcion += '<li>marca: '+producto.marca+'</li>';
                            descripcion += '<li>detalles: '+producto.detalles+'</li>';
                            
                                template += `
                                    <tr productId="${producto.id}">
                                        <td>${producto.id}</td>
                                        <td>
                                            <a href="#" class="producto_item"> ${producto.nombre}</a>
                                        </td>
                                        <td><ul>${descripcion}</ul></td>
                                        <td>
                                            <button class="product-delete btn btn-danger">
                                                Eliminar
                                            </button>
                                        </td>
                                    </tr>
                                `;
                            }); 
                            // SE INSERTA LA PLANTILLA EN EL ELEMENTO CON ID "productos"
                            document.getElementById("products").innerHTML = template;
                }
            })
        }
        // ELIMINAR PRODUCTOS
        $(document).on('click', '.product-delete',function(){
            if(confirm('¿Estas seguro de querer eliminarlo?')){
                let element = $(this)[0].parentElement.parentElement;
                let id = $(element).attr('productId');
                $.post('./backend/product-delete.php',{id},function(response) {
                    let respuesta = JSON.parse(response);
                    // SE CREA UNA PLANTILLA PARA CREAR INFORMACIÓN DE LA BARRA DE ESTADO
                    let template_bar = '';
                    template_bar += `
                                <li style="list-style: none;">status: ${respuesta.status}</li>
                                <li style="list-style: none;">message: ${respuesta.message}</li>
                            `;

                    // SE HACE VISIBLE LA BARRA DE ESTADO
                    document.getElementById("product-result").className = "card my-4 d-block";
                    // SE INSERTA LA PLANTILLA PARA LA BARRA DE ESTADO
                    document.getElementById("container").innerHTML = template_bar;
                    listaProducto(); 
                })
            }
        })

        //MODIFICAR PRODUCTOS
        $(document).on('click', '.producto_item', function() {
            let element = $(this)[0].parentElement.parentElement;
            let id = $(element).attr('productId');
            //console.log(id);
            $.post('./backend/product-single.php', {id}, function(response){
                //console.log(response);
                let productos = JSON.parse(response);
                //console.log(productos);

                $('#form-nombre').val(productos.nombre); //# elemento de html por su id
                $('#form-id').val(productos.id);
                $('#form-marca').val(productos.marca);
                $('#form-modelo').val(productos.modelo);
                $('#form-precio').val(productos.precio);
                $('#form-detalles').val(productos.detalles);
                $('#form-unidades').val(productos.unidades);
                $('#form-imagen').val(productos.imagen);
                
                edit = true;
            });
            
        });

        // BUSCAR NOMBRE
        $('#form-nombre').keyup(function(e) {
            if($('#form-nombre').val()){
                let search = $('#form-nombre').val();
                $.ajax({
                    url:'./backend/validar-nombre.php',
                    type:'GET',
                    data: { search },
                    success: function(response){
                        let productos = JSON.parse(response);
                        //console.log(productos);
                        if(Object.keys(productos).length > 0)
                        {
                            $('#container').html('<li>El nombre del producto ya existe.</il>');
                            // SE HACE VISIBLE LA BARRA DE ESTADO con jquery
                            $('#product-result').show();
                        }
                        else{
                            $('#container').html('<li>Correcto. No existe ese nombre.</il>');
                            // SE HACE VISIBLE LA BARRA DE ESTADO con jquery
                            $('#product-result').show();
                        }
                    }
            });
            }
        })
    })

