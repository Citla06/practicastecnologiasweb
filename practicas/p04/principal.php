<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "htt://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
<head>
    <meta http-equiv="Content-Type" content=" text/html; IE=edge; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Practica 4</title>
</head>
<body>
    <?php
        error_reporting(E_ERROR);
        include("p04_funciones.php");
        
        //Ejercicio 1
        echo "<h3>Ejercicio 1</h3>";
        $numero = $_GET["numero"];
        multiplos($numero);

        //Ejercicio2
        echo "<h3>Ejercicio 2</h3>";
        echo "&nbsp;Impar &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Par &nbsp;&nbsp;&nbsp;&nbsp;Impar <br>";
        matriz();

        //Ejercicio3
        echo "<h3>Ejercicio 3</h3>";
        $numero = $_GET["numero"];
        echo "<i><b>Ciclo While: </b></i>";
        primerEntero($numero);
        echo "<i><br><b>Ciclo Do-While: </b></i>";
        primerEntero_do($numero);

        //Ejercicio4
        echo "<h3>Ejercicio 4</h3>";
        indices();
    ?>
    <!-- Ejercicio 5 -->
    <h3>Ejercicio 5</h3>
    <form action="ejercicio05.php" method="post">
        <p>Sexo:
            <input type="radio" name="sexo" value="hombre"/> Hombre
            <input type="radio" name="sexo" value="femenino"/> Mujer
        </p>
        <p>Edad: <input type="text" name="edad" /></p>
        <p><input type="submit" /></p>
    </form>

    <!-- Ejercicio 6 -->
    <h3>Ejercicio 6</h3>
    <form action="ejercicio06.php" method="post">
        <p>Consultar Auto: 
            <input type="radio" name="auto" value="todos"/> Mostrar Todos
            <br>
            <input type="radio" name="auto" value="matricula"/> Buscar Matricula 
            <input type="text" name="clave" />
            <p><input type="submit" /></p>
        </p>
    </form>
</body>
</html>