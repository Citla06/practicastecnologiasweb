<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "htt://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
<head>
    <meta http-equiv="Content-Type" content=" text/html; IE=edge; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Practica 4 - Ejercicio 6</title>
</head>
<body>
    <?php
        error_reporting(E_ERROR);
        $autos = array(
            'YCV1101' => array(
                'Auto' => array(
                    'marca' => 'Nissan',
                    'modelo' => '2021',
                    'tipo' => 'camioneta'
                ),
                'Propietario' => array(
                    'nombre' => 'Victoria Muñoz',
                    'ciudad' => 'Panotla,Tlax',
                    'direccion' => 'And. Laureles, Panotla'
                )
            ),
            'LMN1402' => array(
                'Auto' => array(
                    'marca' => 'Honda',
                    'modelo' => '2021',
                    'tipo' => 'hachback'
                ),
                'Propietario' => array(
                    'nombre' => 'Jose Sanchez',
                    'ciudad' => 'Huamantla,Tlax',
                    'direccion' => '20 de Nov, Huamantla'
                )
            ),
            'PZY9403' => array(
                'Auto' => array(
                    'marca' => 'Toyota',
                    'modelo' => '2022',
                    'tipo' => 'sedan'
                ),
                'Propietario' => array(
                    'nombre' => 'Yurive Lopez',
                    'ciudad' => 'Panotla,Tlax',
                    'direccion' => 'And. Laureles, Panotla'
                )
            ),
            'WAL8904' => array(
                'Auto' => array(
                    'marca' => 'Mazda',
                    'modelo' => '2019',
                    'tipo' => 'camioneta'
                ),
                'Propietario' => array(
                    'nombre' => 'Hebert Atonal',
                    'ciudad' => 'San Luis Teolocholco,Tlax',
                    'direccion' => 'Calle Vicente, Teolocholco'
                )
            ),
            'LAN2605' => array(
                'Auto' => array(
                    'marca' => 'Ford',
                    'modelo' => '2018',
                    'tipo' => 'sedan'
                ),
                'Propietario' => array(
                    'nombre' => 'Martin Paniagua',
                    'ciudad' => 'Tuxtla, Chiapas',
                    'direccion' => 'Calle Benito, Tuxtla'
                )
            ),
            'WAN1406' => array(
                'Auto' => array(
                    'marca' => 'Nissan',
                    'modelo' => '2019',
                    'tipo' => 'hachback'
                ),
                'Propietario' => array(
                    'nombre' => 'Angeles Lopez',
                    'ciudad' => 'Atlixco, Puebla',
                    'direccion' => 'Av. Libertad, Atlixco'
                )
            ),
            'HYM6607' => array(
                'Auto' => array(
                    'marca' => 'Fiat',
                    'modelo' => '2020',
                    'tipo' => 'camioneta'
                ),
                'Propietario' => array(
                    'nombre' => 'Alheli Moreno',
                    'ciudad' => 'Puebla, Pue',
                    'direccion' => 'Bosques de Amaluca, Galaxia'
                )
            ),
            'GHO4508' => array(
                'Auto' => array(
                    'marca' => 'Volkswagen',
                    'modelo' => '2019',
                    'tipo' => 'sedan'
                ),
                'Propietario' => array(
                    'nombre' => 'Gael Sanchez',
                    'ciudad' => 'Apizaco,Tlax',
                    'direccion' => 'Calle Miraflores, Apizaco'
                )
            ),
            'WAL8909' => array(
                'Auto' => array(
                    'marca' => 'Mazda',
                    'modelo' => '2020',
                    'tipo' => 'camioneta'
                ),
                'Propietario' => array(
                    'nombre' => 'Hebert Atonal',
                    'ciudad' => 'Apizaco, Tlax',
                    'direccion' => 'Calle Miraflores, Apizaco'
                )
            ),
            'HJK7510' => array(
                'Auto' => array(
                    'marca' => 'Ford',
                    'modelo' => '2018',
                    'tipo' => 'heachback'
                ),
                'Propietario' => array(
                    'nombre' => 'Yarely Calva',
                    'ciudad' => 'Tezoquipan,Tlax',
                    'direccion' => 'Calle Rosas, San Jorge'
                )
            ),
            'BPO9811' => array(
                'Auto' => array(
                    'marca' => 'Honda',
                    'modelo' => '2019',
                    'tipo' => 'heachback'
                ),
                'Propietario' => array(
                    'nombre' => 'Ariadna Lopez',
                    'ciudad' => 'Atlixco, Pue',
                    'direccion' => 'Calle Sauces, Metepec'
                )
            ),
            'PLO6312' => array(
                'Auto' => array(
                    'marca' => 'Toyota',
                    'modelo' => '2021',
                    'tipo' => 'camioneta'
                ),
                'Propietario' => array(
                    'nombre' => 'Monserat Muñoz',
                    'ciudad' => 'Puebla, Pue',
                    'direccion' => 'Calle Miraflores, Lomas de San Miguel'
                )
            ),
            'ASD5613' => array(
                'Auto' => array(
                    'marca' => 'Honda',
                    'modelo' => '2020',
                    'tipo' => 'sedan'
                ),
                'Propietario' => array(
                    'nombre' => 'Alejandro Ramires',
                    'ciudad' => 'Cuernavaca, Morelos',
                    'direccion' => 'Calle Independecia, Cuernavaca'
                )
            ),
            'RTF1214' => array(
                'Auto' => array(
                    'marca' => 'Suzuki',
                    'modelo' => '2018',
                    'tipo' => 'hachback'
                ),
                'Propietario' => array(
                    'nombre' => 'Lizbet Muñoz',
                    'ciudad' => 'Chignahuapan, Puebla',
                    'direccion' => 'Av.Hidalgo , San Antonio'
                )
            ),
            'ZRT4815' => array(
                'Auto' => array(
                    'marca' => 'Nissa',
                    'modelo' => '2021',
                    'tipo' => 'camioneta'
                ),
                'Propietario' => array(
                    'nombre' => 'Cecilia Perez',
                    'ciudad' => 'Chignahuapan, Puebla',
                    'direccion' => 'Calle 20 Noviembre, Chignahuapan'
                )
            )
        );

        //opcion todos
        $opcion = $_POST['auto'];

        if($opcion == "todos"){
            foreach($autos as $matricula => $auto)
            {
                echo "<h3> Automovil </h3>";
                    echo "Matricula: ".$matricula."<br>";
                    echo "Marca: ".$auto['Auto']['marca']."<br>";
                    echo "Modelo: ".$auto['Auto']['modelo']."<br>";
                    echo "Tipo: ".$auto['Auto']['tipo']."<br>";
                    echo "Propietario: ".$auto['Propietario']['nombre']."<br>";
                    echo "Ciudad: ".$auto['Propietario']['ciudad']."<br>";
                    echo "Direccion: ".$auto['Propietario']['direccion']."<br>";
            }
        }
        else{
            $clave = $_POST["clave"];
            foreach($autos as $matricula => $auto)
            {
                if($clave == $matricula){
                    echo "<h3> Automovil </h3>";
                    echo "Matricula: ".$matricula."<br>";
                    echo "Marca: ".$auto['Auto']['marca']."<br>";
                    echo "Modelo: ".$auto['Auto']['modelo']."<br>";
                    echo "Tipo: ".$auto['Auto']['tipo']."<br>";
                    echo "Propietario: ".$auto['Propietario']['nombre']."<br>";
                    echo "Ciudad: ".$auto['Propietario']['ciudad']."<br>";
                    echo "Direccion: ".$auto['Propietario']['direccion']."<br>";

                }
            }
        }
    ?>
</body>
</html>