<?php

namespace BaseDatos;
use BaseDatos\Database;
require_once __DIR__.'/database.php';


// Se hereda de clase 
class Productos extends Database{
    
    private $response;

    public function __construct($nameBD = 'marketzone'){ ///Constructor
        $this->response = "";
        parent :: __construct($nameBD);
    }

    // Función getResponse
    public function getResponse(){
        return json_encode($this->response, JSON_PRETTY_PRINT);
    }

    // Función add
    public function add($post){
            $id = $post['id'];
            $nombre = $post['name'];
            $marca  = $post['marca'];
            $modelo = $post['modelo'];
            $precio = $post['precio'];
            $detalles = $post['detalles'];
            $unidades = $post['unidades'];
            $imagen   = $post['imagen'];
            
            $this->response = array(
                'status'  => 'error',
                'message' => 'Ya existe un producto con ese nombre'
            );

            //SE ASUME QUE LOS DATOS YA FUERON VALIDADOS ANTES DE ENVIARSE
            $sql = "SELECT * FROM productos WHERE nombre = '{$nombre}' AND eliminado = 0";
            $result = $this->conexion->query($sql);
            
            if ($result->num_rows == 0) {
                $this->conexion->set_charset("utf8");
                $sql = "INSERT INTO productos VALUES (null, '{$nombre}', '{$marca}','{$modelo}','{$precio}','{$detalles}','{$unidades}','{$imagen}', 0)";
                if($this->conexion->query($sql)){
                    $this->response['status'] =  "success";
                    $this->response['message'] =  "Producto agregado";
                } else {
                    $this->response['message'] = "ERROR: No se ejecuto $sql. " . mysqli_error($this->conexion);
                }
            
            // Cierra la conexion
            $this->conexion->close();
            }

    }

    //Function list
    public function list(){
        // SE CREA EL ARREGLO QUE SE VA A DEVOLVER EN FORMA DE JSON
        $this->response = array();

        // SE REALIZA LA QUERY DE BÚSQUEDA Y AL MISMO TIEMPO SE VALIDA SI HUBO RESULTADOS
        if ( $result = $this->conexion->query("SELECT * FROM productos WHERE eliminado = 0") ) {
            // SE OBTIENEN LOS RESULTADOS
            $rows = $result->fetch_all(MYSQLI_ASSOC);

            if(!is_null($rows)) {
                // SE CODIFICAN A UTF-8 LOS DATOS Y SE MAPEAN AL ARREGLO DE RESPUESTA
                foreach($rows as $num => $row) {
                    foreach($row as $key => $value) {
                        $this->response[$num][$key] = $value;
                    }
                }
            }
            $result->free();
        } else {
            die('Query Error: '.mysqli_error($this->conexion));
        }
        $this->conexion->close();
    }

    //Función search
    public function search($get){
        // SE CREA EL ARREGLO QUE SE VA A DEVOLVER EN FORMA DE JSON
        $this->response = array();
        // SE VERIFICA HABER RECIBIDO EL ID
        if( isset($get['search']) ) {
            $search = $get['search'];
            // SE REALIZA LA QUERY DE BÚSQUEDA Y AL MISMO TIEMPO SE VALIDA SI HUBO RESULTADOS
            $sql = "SELECT * FROM productos WHERE (id = '{$search}' OR nombre LIKE '%{$search}%' OR marca LIKE '%{$search}%' OR detalles LIKE '%{$search}%') AND eliminado = 0";
            if ( $result = $this->conexion->query($sql) ) {
                // SE OBTIENEN LOS RESULTADOS
                $rows = $result->fetch_all(MYSQLI_ASSOC);

                if(!is_null($rows)) {
                    // SE CODIFICAN A UTF-8 LOS DATOS Y SE MAPEAN AL ARREGLO DE RESPUESTA
                    foreach($rows as $num => $row) {
                        foreach($row as $key => $value) {
                            $this->response[$num][$key] = $value;
                        }
                    }
                }
                $result->free();
            } else {
                die('Query Error: '.mysqli_error($this->conexion));
            }
            $this->conexion->close();
        } 
    }

    public function delete($post){
        // SE CREA EL ARREGLO QUE SE VA A DEVOLVER EN FORMA DE JSON
        $this->response = array(
            'status'  => 'error',
            'message' => 'La consulta falló'
        );
        // SE VERIFICA HABER RECIBIDO EL ID
        if( isset($post['id']) ) 
        {
            $id = $post['id'];
            // SE REALIZA LA QUERY DE BÚSQUEDA Y AL MISMO TIEMPO SE VALIDA SI HUBO RESULTADOS
            $sql = "UPDATE productos SET eliminado=1 WHERE id = {$id}";
            if ( $this->conexion->query($sql) ) {
                $this->response['status'] =  "success";
                $this->response['message'] =  "Producto eliminado";
            } else {
                $this->response['message'] = "ERROR: No se ejecuto $sql. " . mysqli_error($this->conexion);
            }
            $this->conexion->close();
        } 
    }

    //Función single
    public function single($post){
        $id = $post['id'];

        $query = "SELECT * FROM productos WHERE id = {$id}";
        $result = $this->conexion->query($query);
        if(!$result){
            die('Error de query');
        }

        $this->response = array();
        while($row = $result->fetch_array()) {
            $this->response = array(
                'id' => $row['id'],
                'nombre' => $row['nombre'],
                'precio' => $row['precio'],
                'unidades' => $row['unidades'],
                'modelo' => $row['modelo'],
                'marca' => $row['marca'],
                'detalles' => $row['detalles'],
                'imagen' => $row['imagen']
            );
        }
    } 

    public function edit($post){
        $id = $post['id'];
    $nombre = $post['name'];
    $marca  = $post['marca'];
    $modelo = $post['modelo'];
    $precio = $post['precio'];
    $detalles = $post['detalles'];
    $unidades = $post['unidades'];
    $imagen   = $post['imagen'];
    
    $this->response = array(
        'status'  => 'error',
        'message' => 'Ya existe un producto con ese nombre'
    );
    
            $sql = "UPDATE productos SET nombre ='{$nombre}', marca= '{$marca}', modelo='{$modelo}', precio= {$precio}, detalles='{$detalles}', unidades ={$unidades}, imagen ='{$imagen}',eliminado = 0
            WHERE id='{$id}'" ;
            if($this->conexion->query($sql)){
                $this->response['status'] =  "success";
                $this->response['message'] =  "Producto actualizado";
            } else {
                $this->response['message'] = "ERROR: No se ejecuto $sql. " . mysqli_error($this->conexion);
            }

        // $result->free();
        // Cierra la conexion
        $this->conexion->close();
    } 
    
    
}
?>
