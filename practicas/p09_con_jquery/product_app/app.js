// JSON BASE A MOSTRAR EN FORMULARIO
var baseJSON = {
    "precio": 0.0,
    "unidades": 1,
    "modelo": "XX-000",
    "marca": "NA",
    "detalles": "NA",
    "imagen": "img/default.png"
    };

function init() {
    /**
     * Convierte el JSON a string para poder mostrarlo
     * ver: https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/JSON
     */
    var JsonString = JSON.stringify(baseJSON,null,2);
    document.getElementById("description").value = JsonString;
}

// FUNCION PARA BUSCAR LOS PRODUCTOS
// var client = getXMLHttpRequest();
//     client.open('POST', './backend/product-search.php?search='+search, true);
//     client.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
//     client.onreadystatechange = 

$(document).ready(function(){
        let edit = false;
        console.log('jQuery is working');
        //LISTAR LOS PRODUCTOS
        listaProducto();
        init();
        $('#search').keyup(function(e) {
            if($('#search').val()){
                let search = $('#search').val();
                $.ajax({
                    url:'./backend/product-search.php',
                    type:'GET',
                    data: { search },
                    success: function(response){
                        let productos = JSON.parse(response);
                        let template = '';
                        let template_bar = '';
                        productos.forEach(producto => {
                            let descripcion = '';
                            descripcion += '<li>precio: '+producto.precio+'</li>';
                            descripcion += '<li>unidades: '+producto.unidades+'</li>';
                            descripcion += '<li>modelo: '+producto.modelo+'</li>';
                            descripcion += '<li>marca: '+producto.marca+'</li>';
                            descripcion += '<li>detalles: '+producto.detalles+'</li>';
                        
                            template += `
                                <tr productId="${producto.id}">
                                    <td>${producto.id}</td>
                                    <td>${producto.nombre}</td>
                                    <td><ul>${descripcion}</ul></td>
                                    <td>
                                        <button class="product-delete btn btn-danger" onclick="eliminarProducto()">
                                            Eliminar
                                        </button>
                                    </td>
                                </tr>
                            `;

                            template_bar += `
                                <li>${producto.nombre}</il>
                            `;
                        });
                        // SE HACE VISIBLE LA BARRA DE ESTADO
                        document.getElementById("product-result").className = "card my-4 d-block";
                        // SE INSERTA LA PLANTILLA PARA LA BARRA DE ESTADO
                        document.getElementById("container").innerHTML = template_bar;  
                        // SE INSERTA LA PLANTILLA EN EL ELEMENTO CON ID "productos"
                        document.getElementById("products").innerHTML = template;
                        init();
                    }
            });
            }
        })

        // AGREGAR DATOS
        $('#product-form').submit(function(e){
            //console.log('Enviando');
            const postData = {
                name: $('#name').val(),
                description: $('#description').val(),
                id: $('#productoId').val()
            };
            

            let url = edit == false ? './backend/product-add.php' : './backend/product-edit.php';
            console.log(url);

            $.post(url, postData, function(response) {
                console.log(response);
                let respuesta = JSON.parse(response);
                // SE CREA UNA PLANTILLA PARA CREAR INFORMACIÓN DE LA BARRA DE ESTADO
                let template_bar = '';
                template_bar += `
                            <li style="list-style: none;">status: ${respuesta.status}</li>
                            <li style="list-style: none;">message: ${respuesta.message}</li>
                        `;
                
                // SE HACE VISIBLE LA BARRA DE ESTADO
                document.getElementById("product-result").className = "card my-4 d-block";
                // SE INSERTA LA PLANTILLA PARA LA BARRA DE ESTADO
                document.getElementById("container").innerHTML = template_bar;   
                listaProducto();
                //$('#product-form').trigger('reset');
                init();
            });
            e.preventDefault();
        });


        //LISTAR LOS PRODUCTOS
        function listaProducto(){
            $.ajax({
                url: './backend/product-list.php',
                type: 'GET',
                success: function(response) {
                    const productos = JSON.parse(response);
                    let template = '';
                    productos.forEach(producto => {
                            let descripcion = '';
                            descripcion += '<li>precio: '+producto.precio+'</li>';
                            descripcion += '<li>unidades: '+producto.unidades+'</li>';
                            descripcion += '<li>modelo: '+producto.modelo+'</li>';
                            descripcion += '<li>marca: '+producto.marca+'</li>';
                            descripcion += '<li>detalles: '+producto.detalles+'</li>';
                            
                                template += `
                                    <tr productId="${producto.id}">
                                        <td>${producto.id}</td>
                                        <td>
                                            <a href="#" class="producto_item"> ${producto.nombre}</a>
                                        </td>
                                        <td><ul>${descripcion}</ul></td>
                                        <td>
                                            <button class="product-delete btn btn-danger">
                                                Eliminar
                                            </button>
                                        </td>
                                    </tr>
                                `;
                            }); 
                            // SE INSERTA LA PLANTILLA EN EL ELEMENTO CON ID "productos"
                            document.getElementById("products").innerHTML = template;
                }
            })
        }
        // ELIMINAR PRODUCTOS
        $(document).on('click', '.product-delete',function(){
            if(confirm('¿Estas seguro de querer eliminarlo?')){
                let element = $(this)[0].parentElement.parentElement;
                let id = $(element).attr('productId');
                $.post('./backend/product-delete.php',{id},function(response) {
                    let respuesta = JSON.parse(response);
                    // SE CREA UNA PLANTILLA PARA CREAR INFORMACIÓN DE LA BARRA DE ESTADO
                    let template_bar = '';
                    template_bar += `
                                <li style="list-style: none;">status: ${respuesta.status}</li>
                                <li style="list-style: none;">message: ${respuesta.message}</li>
                            `;

                    // SE HACE VISIBLE LA BARRA DE ESTADO
                    document.getElementById("product-result").className = "card my-4 d-block";
                    // SE INSERTA LA PLANTILLA PARA LA BARRA DE ESTADO
                    document.getElementById("container").innerHTML = template_bar;
                    listaProducto();
                    init(); 
                })
            }
        })

        //MODIFICAR PRODUCTOS
        $(document).on('click', '.producto_item', function() {
            let element = $(this)[0].parentElement.parentElement;
            let id = $(element).attr('productId');
            console.log(id);
            $.post('./backend/product-single.php', {id}, function(response){
                console.log(response);
                let productos = JSON.parse(response);
                let info = {
                    "precio": productos.precio,
                    "unidades": productos.unidades,
                    "modelo": productos.modelo,
                    "marca": productos.marca,
                    "detalles": productos.detalles,
                    "imagen": productos.imagen
                }
                
                var infoString = JSON.stringify(info,null,2);
                $('#name').val(productos.nombre);
                $('#description').val(infoString);
                $('#productoId').val(productos.id);

                opc2=opc =$('#name').val(productos.nombre);

                if(opc2 == opc){
                    let template_bar = '';
                    template_bar += `
                                <li style="list-style: none;">status: ${productos.status}</li>
                                <li style="list-style: none;">message: ${productos.message}</li>
                            `;

                    // SE HACE VISIBLE LA BARRA DE ESTADO
                    document.getElementById("product-result").className = "card my-4 d-block";
                    // SE INSERTA LA PLANTILLA PARA LA BARRA DE ESTADO
                }
                else{
                    console.log("AA");
                }
                edit = true;
            });
            
        });
    });

